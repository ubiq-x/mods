
// --------------------------------------------------------------------
function init() {
	audioTick = document.getElementById("audio-tick");
	
	// process page call arguments:
	processQS();
	
	// init stimuli (fixed for training and random for each of 8 conditions):
	setTrainingStimuli();
	for (var i=0; i < STIMULI_PER_CONDITION_CNT; i++) generateStimulus(3);
	for (var i=0; i < STIMULI_PER_CONDITION_CNT; i++) generateStimulus(4);
	for (var i=0; i < STIMULI_PER_CONDITION_CNT; i++) generateStimulus(5);
	for (var i=0; i < STIMULI_PER_CONDITION_CNT; i++) generateStimulus(6);
	
	// generate random (test) stimuli order:
	for (var i=0; i < stimuliTest.length; i++) order[i] = i;
	order.sort(randOrd);
	
	// ui:
	initUI();
}


// --------------------------------------------------------------------
function setTrainingStimuli() {
	stimuliTraining[0] = {
		digitCnt: 3,
		str: "acb4efb5iajb1",
		charCnt: 13,
		digitIdx: [3,7,12],
		answers: [],
		result: RESULT_NONE,
		digitResults: []
	};
	
	stimuliTraining[1] = {
		digitCnt: 4,
		str: "jab5eaji1dfe0aife6",
		charCnt: 18,
		digitIdx: [3,8,12,17],
		answers: [],
		result: RESULT_NONE,
		digitResults: []
	};
	
	stimuliTraining[2] = {
		digitCnt: 6,
		str: "fahg1hdbc3egj7jhc3cdag3echg7",
		charCnt: 28,
		digitIdx: [4,9,13,17,22,27],
		answers: [],
		result: RESULT_NONE,
		digitResults: []
	};
}


// --------------------------------------------------------------------
function randOrd() { return Math.round(Math.random()) - 0.5; }


// --------------------------------------------------------------------
function processQS() {
	// decoding parameters:
	var qs = new Array();
	var query = window.location.search.substring(1);
	var parms = query.split('&');
	for (var i=0; i < parms.length; i++) {
		var pos = parms[i].indexOf('=');
		if (pos > 0) {
			var key = parms[i].substring(0, pos);
			var val = parms[i].substring(pos+1);
			qs[key] = val;
		}
	}
	
	// checking and updating variables:
	if (qs["lrate"] != undefined && !isNaN(parseInt(qs["lrate"]))) RATE_LETTER = parseInt(qs["lrate"]);
	if (qs["drate"] != undefined && !isNaN(parseInt(qs["drate"]))) RATE_DIGIT = parseInt(qs["drate"]);
	if (qs["stimcnt"] != undefined && !isNaN(parseInt(qs["stimcnt"]))) STIMULI_PER_CONDITION_CNT = parseInt(qs["stimcnt"]);
	if (qs["font"] != undefined && !isNaN(parseInt(qs["font"]))) FONT_SIZE = parseInt(qs["font"]);
}


// --------------------------------------------------------------------
/*
 * Generates a single stimulus for given condition, i.e. amount of 
 * digits 'digitCnt'.
 */
function generateStimulus(digitCnt) {
	var s = {  // stimulus
		digitCnt: digitCnt,
		str: "",  // string of chars and letters
		charCnt: 0,  // the total number of characters in the stimulus; for performance purposes
		digitIdx: [],
		answers: [],  // answers provided by the user
		result: RESULT_NONE,
		digitResults: []
	};
	
	for (var i=0; i < digitCnt; i++) {
		var len = (Math.random() <= 0.5 ? STR_LEN : STR_LEN + 1);  // length of the letters string
		
		s.str += generateLetters(len);  // generate a letters string
		s.str += Math.floor(Math.random() * 10);  // adding a digit
		s.digitIdx.push(s.str.length - 1);  // saving digit's idx
	}
	s.charCnt = s.str.length;
	
	stimuliTest.push(s);
}


// --------------------------------------------------------------------
/*
 * Generates a string with 'n' letters taken from 'LETTERS' var in 
 * random order without repeating.
 */
function generateLetters(n) {
	var res = "";
	var letterCnt = LETTERS.length;
	
	var rand = Math.floor(Math.random() * letterCnt);
	do {
		while (res.indexOf(LETTERS[rand]) != -1) rand = Math.floor(Math.random() * letterCnt);
		res += LETTERS[rand];
	} while (res.length < n);
	
	return res;
}


// --------------------------------------------------------------------
function initUI() {
	document.onkeydown = docOnKeyDown;
	
	// adding input elements:
	var div = ge("stimulus");
	for (var i=0; i < MAX_STIMULUS_LEN; i++) {
		var input = ce("input", div, "char char-off");
		input.disabled = true;
		input.onkeydown = inputOnKeyDown;
		input.setAttribute("maxlength", "1");
		input.style.fontSize = FONT_SIZE + "px";
		input.style.width = FONT_SIZE + 8;
		
		inputs.push(input);
	}
	
	ui.btnOk = ge("btnOk");
}
