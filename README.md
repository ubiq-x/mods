# Modified Digit Span (MODS) Task

## Launching
There are two ways of launching the task.  To launch it off the internet, simply navigate to:
[https://ubiq-x.gitlab.io/mods](https://ubiq-x.gitlab.io/mods/)

If you don't have internet access on your experimental machine or simply prefer to run in localy, clone this repository:
```
git clone https://gitlab.com/ubiq-x/mods
```
and open the ```index.html``` in your browser.

Either way, you will want to enter full-screen mode in your browser to get best user experience.


## References
Daily, L.Z., Lovett, M.C., Reder, L.M. (2001) Modeling individual differences in working memory performance: A source activation account.  _Cognitive Science: A Multi- disciplinary Journal_, 25(3), 315–353.


## Citing
Loboda, T.D. (2008).  Modified digit span (MODS) task [Web application].  Available at: _https://ubiq-x.gitlab.io/mods_


## License
This project is licensed under the [BSD License](LICENSE.md).
