
// --------------------------------------------------------------------
function startTraining() {
	hide("startBtns");
	show("stimulus");
	show("okBtn");
}


// --------------------------------------------------------------------
function startTest() {
	hide("startBtns");
	show("stimulus");
	show("okBtn");
	
	state.stimulusIdx = -1;  // nextStimulus() increments it
	
	ui.btnOk.value = "I'm ready";
	ui.btnOk.onclick = nextStimulus;
	enable(ui.btnOk);
	ui.btnOk.focus();
}


// --------------------------------------------------------------------
function nextStimulus() {
	disable(ui.btnOk);
	ui.btnOk.value = "Next digit";
	ui.btnOk.onclick = nextDigit;
	
	state.stimulusIdx++;
	state.charIdx = 0;
	state.digitIdx = 0;
	
	timer = setTimeout("tick()", RATE);
}


// --------------------------------------------------------------------
function tick() {
	var s = stimuli[state.stimulusIdx];
	
	if (state.charIdx < s.charCnt-1) timer = setTimeout("tick()", RATE);
	else timer = setTimeout("initAnswerPrompt()", RATE);
	
	inputs[state.charIdx].value = s.str[state.charIdx];
	state.charIdx++;
}


// --------------------------------------------------------------------
function initAnswerPrompt() {
	disable(ui.btnOk);
	var s = stimuli[state.stimulusIdx];
	
	for (var i=0; i < s.charCnt; i++) inputs[i].value = "";
	for (var i=1; i < s.digitIdx.length; i++) inputs[s.digitIdx[i]].className = "char char-on";
		// skipping the first input -- it will be activated anyway below
	
	var input = inputs[s.digitIdx[state.digitIdx]];
	input.className = "char char-on char-on-active";
	enable(input);
	input.focus();
}


// --------------------------------------------------------------------
function nextDigit() {
	disable(ui.btnOk);
	var s = stimuli[state.stimulusIdx];
	
	// deactivate the current input:
	var input = inputs[s.digitIdx[state.digitIdx]];
	input.className = "char char-off";
	disable(input);
	
	state.digitIdx++;
	
	// still more digits to ask for:
	if (state.digitIdx < s.digitIdx.length) {
		input = inputs[s.digitIdx[state.digitIdx]];
		input.className = "char char-on char-on-active";
		enable(input);
		input.focus();
	}
	// all digits provided:
	else processAnswer();
	
	// Ok button label:
	if (state.digitIdx == s.digitIdx.length-1) ui.btnOk.value = "I'm Done";
}


// --------------------------------------------------------------------
function processAnswer() {
	var s = stimuli[state.stimulusIdx];
	
	// checking the result and resetting inputs:
	var allCorrect = true;  // are all digits correct
	for (var i=0; i < s.digitIdx.length; i++) {
		var correct = (inputs[s.digitIdx[i]].value == s.str[s.digitIdx[i]]);  // is this digit correct
		s.digitResults[i] = (correct ? RESULT_RIGHT : RESULT_WRONG);
		allCorrect = allCorrect && correct;
		inputs[s.digitIdx[i]].value = "";
	}
	s.result = (allCorrect ? RESULT_RIGHT : RESULT_WRONG);
	
	// still more stimuli to be administered:
	if (state.stimulusIdx < stimuli.length-1) {
		ui.btnOk.value = "I'm ready";
		ui.btnOk.onclick = nextStimulus;
		enable(ui.btnOk);
		ui.btnOk.focus();
	}
	// reached the end of the experiment:
	else displayResults();
}


// --------------------------------------------------------------------
function displayResults() {
	hide("stimulus");
	hide("okBtn");
	
	var results = [];
	for (var i=0, l=stimuli.length; i < l; i++) {
		var s = stimuli[i];
		results.push("" + s.digitCnt + "," + s.strLen + "," + s.str + "," + s.result + "," + s.digitResults.join(","));
	}
	ge("resutlsText").value = results.join("\n");
	
	show("results");
}
