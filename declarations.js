
var RATE_LETTER = 500;  // rate of display [ms]
var RATE_DIGIT = 910;
var FONT_SIZE = 14;
var LETTERS = "abcdefghij";

var STR_LEN = 3;

var STIMULI_PER_CONDITION_CNT = 4;
var MAX_STIMULUS_LEN = 30;  // (6 strings) * (4 letters per each) + (6 digits)

var NONE = -1;

var RESULT_NONE = -1;
var RESULT_WRONG = 0;
var RESULT_RIGHT = 1;

var UNLOCK_PASSWD = "yo";

var stimuliTraining = [];
var stimuliTest = [];
var order = [];  // order of stimuli administration
var inputs = [];  // input elements

var ui = {
	btnOk: null
};

var state = {  // hold info on the current state of the experiment
	test: false,  // test or training
	stimuli: null,  // pointer to the stimuli array (different for training and test)
	stimulusIdx: NONE,
	charIdx: NONE,
	digitIdx: NONE
};

var timer = null;
