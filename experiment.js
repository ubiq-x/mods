// --------------------------------------------------------------------
/*
 * 'test' indicates if it's a training (false) or test (true).
 */
function startExperiment(test) {
	hide("btnStartOver");
	hide("btnUnlock")
	hide("startBtns");
	show("stimulus");
	show("okBtn");
	
	state.test = test;
	state.stimuli = (test ? stimuliTest : stimuliTraining);
	state.stimulusIdx = -1;  // nextStimulus() increments it
	
	ui.btnOk.value = "I'm ready";
	ui.btnOk.onclick = nextStimulus;
	enable(ui.btnOk);
	ui.btnOk.focus();
}


// --------------------------------------------------------------------
function nextStimulus() {
	disable(ui.btnOk);
	ui.btnOk.value = "Next digit";
	ui.btnOk.onclick = nextDigit;
	
	state.stimulusIdx++;
	state.charIdx = 0;
	state.digitIdx = 0;
	
	timer = setTimeout("tick()", RATE_LETTER);
}


// --------------------------------------------------------------------
function tick() {
	audioTick.play();
	var s = state.stimuli[state.stimulusIdx];
	
	var func = (state.charIdx < s.charCnt-1 ? "tick()" : "initAnswerPrompt()");
	var rate = (state.stimuli[state.stimulusIdx].digitIdx.indexOf(state.charIdx) === -1 ? RATE_LETTER : RATE_DIGIT);
	
	timer = setTimeout(func, rate);
	
	inputs[state.charIdx].value = s.str[state.charIdx];
	state.charIdx++;
}


// --------------------------------------------------------------------
function initAnswerPrompt() {
	disable(ui.btnOk);
	var s = state.stimuli[state.stimulusIdx];
	
	for (var i=0; i < s.charCnt; i++) inputs[i].value = "";
	for (var i=1; i < s.digitIdx.length; i++) inputs[s.digitIdx[i]].className = "char char-on";
		// skipping the first input -- it will be activated anyway below
	
	var input = inputs[s.digitIdx[state.digitIdx]];
	input.className = "char char-on char-on-active";
	enable(input);
	input.focus();
}


// --------------------------------------------------------------------
function nextDigit() {
	disable(ui.btnOk);
	var s = state.stimuli[state.stimulusIdx];
	
	// deactivate the current input:
	var input = inputs[s.digitIdx[state.digitIdx]];
	input.className = "char char-off";
	disable(input);
	
	state.digitIdx++;
	
	// still more digits to ask for:
	if (state.digitIdx < s.digitIdx.length) {
		input = inputs[s.digitIdx[state.digitIdx]];
		input.className = "char char-on char-on-active";
		enable(input);
		input.focus();
	}
	// all digits provided:
	else processAnswer();
	
	// Ok button label:
	if (state.digitIdx == s.digitIdx.length-1) ui.btnOk.value = "I'm Done";
}


// --------------------------------------------------------------------
function processAnswer() {
	var s = state.stimuli[state.stimulusIdx];
	
	// storing the result and clearing inputs:
	for (var i=0; i < s.digitIdx.length; i++) {
		s.answers.push(inputs[s.digitIdx[i]].value);
		inputs[s.digitIdx[i]].value = "";
	}
	
	// still more stimuli to be administered:
	if (state.stimulusIdx < state.stimuli.length-1) {
		ui.btnOk.value = "I'm ready";
		ui.btnOk.onclick = nextStimulus;
		enable(ui.btnOk);
		ui.btnOk.focus();
	}
	// reached the end of the experiment:
	else displayResults();
}


// --------------------------------------------------------------------
function displayResults() {
	hide("stimulus");
	hide("okBtn");
	
	var results = [];
	for (var i=0, l=state.stimuli.length; i < l; i++) {
		var s = state.stimuli[i];
		
		// checking the user's answers:
		var allCorrect = true;  // are all digits correct
		for (var j=0; j < s.digitIdx.length; j++) {
			var correct = (s.answers[j] == s.str[s.digitIdx[j]]);  // is this digit correct
			s.digitResults[j] = (correct ? RESULT_RIGHT : RESULT_WRONG);
			allCorrect = allCorrect && correct;
		}
		s.result = (allCorrect ? RESULT_RIGHT : RESULT_WRONG);
		
		// adding to results:
		results.push("" + s.digitCnt + "," + s.str + "," + s.answers.join("") + "," + s.result + "," + s.digitResults.join(","));
	}
	ge("resutlsText").value = results.join("\n");
	
	if (state.test) show("btnUnlock")
	else show("btnStartOver");
	
	show("results");
}


// --------------------------------------------------------------------
function unlockResults() {
	if (prompt("Enter unlock password") != UNLOCK_PASSWD) return;
	
	ge("resutlsText").disabled = false;
	ge("btnUnlock").disabled = true;
}


// --------------------------------------------------------------------
function startOver() {
	hide("results");
	show("startBtns");
}


// --------------------------------------------------------------------
function docOnKeyDown(e) {
	e = getEvent(e);
	var key = (window.event) ? event.keyCode : e.which;
	
	if ((e.ctrlKey && key == 82) || (!e.ctrlKey && key == 116)) return false;  // CTRL+R or F5:
}


// - - - -
function inputOnKeyDown(e) {
	var target = et(e);
	var digitEntered = (target.value.length == 1);
	
	e = getEvent(e);
	var key = (window.event) ? event.keyCode : e.which;
	//alert(key);
	
	if (key == 8) {  // backspace
		disable(ui.btnOk);
		return true;
	}
	
	if (digitEntered && key == 13) {  // enter when the input has a digit already
		nextDigit();
		return false;
	}
	
	if (!e.shiftKey && ((key >= 48 && key <= 57) || key == 88)) {  // numbers 0-9 + letter 'x'
		enable(ui.btnOk);
		return true;
	}
	
	return false;
}
