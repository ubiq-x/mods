
// --------------------------------------------------------------------
function ge(id) { return document.getElementById(id); }  // get element
function sc(id, className) { dg(id).className = className; }  // set class

function hide(id) { ge(id).style.display = "none"; }
function show(id) { ge(id).style.display = "block"; }

function enable(elem) { elem.disabled = false; }
function disable(elem) { elem.disabled = true; }


// --------------------------------------------------------------------
function ce(name, parent, className) {  // create element
	var elem = document.createElement(name);
	if (parent) parent.appendChild(elem);
	if (className) elem.className = className;
	
	return elem;
}


// --------------------------------------------------------------------
function et(e) { e = getEvent(e); return e.target || e.srcElement; }  // event target

function getEvent(e) {
	if (typeof e == 'undefined') e = window.event;
	if (typeof e.layerX == 'undefined') e.layerX = e.offsetX;
	if (typeof e.layerY == 'undefined') e.layerY = e.offsetY;
	
	return e;
}
